<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert
        ([
            [
                'nama'=>'Fitri Ameliani Syawadah',
                'nim'=>'1715015021',
                'kelas'=>'A 17'
            ],
            [
                'nama'=>'Astrid Rian Rahmana',
                'nim'=>'1715015022',
                'kelas'=>'A 17'
            ],
            [
                'nama'=>'Ilham',
                'nim'=>'1715015025',
                'kelas'=>'A 17'
            ],
            [
                'nama'=>'Muhammad Rizky Fadhilah',
                'nim'=>'1715015026',
                'kelas'=>'A 17'
            ],
            [
                'nama'=>'Muhammad Aditya Pratama',
                'nim'=>'1715015028',
                'kelas'=>'A 17'
            ],
            [
                'nama'=>'Yusril Razak Alansa',
                'nim'=>'1715015029',
                'kelas'=>'A 17'
            ],
            [
                'nama'=>'Hawi Natalino',
                'nim'=>'1715015031',
                'kelas'=>'A 17'
            ],
            [
                'nama'=>'Muhammad Taqiyyudin Naufan',
                'nim'=>'1715015032',
                'kelas'=>'A 17'
            ],
            [
                'nama'=>'Federika Hefrasonia Nelle',
                'nim'=>'1715015033',
                'kelas'=>'A 17'
            ],
            [
                'nama'=>'Iga Vita Suri',
                'nim'=>'1715015034',
                'kelas'=>'A 17'
            ],
            [
                'nama'=>'Rizky Wijaya Kusuma',
                'nim'=>'1715015036',
                'kelas'=>'A 17'
            ],
        ]);
    }
}
