<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKartukeluargaTable extends Migration
{
    public function up()
    {
        Schema::create('kartukeluargas', function (Blueprint $table) {
            $table->increments('id_kk');
            $table->string('no_kk');
            $table->unsignedinteger('id_rt');
            $table->timestamps();
            $table->foreign('id_rt')->references('id_rt')->on('rukuntetanggas')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('kartukeluarga');
    }
}
