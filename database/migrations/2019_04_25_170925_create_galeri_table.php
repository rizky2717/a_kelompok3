<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGaleriTable extends Migration
{
    public function up()
    {
        Schema::create('galeris', function (Blueprint $table) {
            $table->increments('id_galeri');
            $table->string('lokasi_galeri');
            $table->unsignedinteger('id_kegiatan');
            $table->timestamps();
            $table->foreign('id_kegiatan')->references('id_kegiatan')->on('kegiatans')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('galeri');
    }
}
