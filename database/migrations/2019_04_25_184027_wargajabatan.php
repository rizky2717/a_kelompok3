<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Wargajabatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wargajabatans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_warga');
            $table->unsignedInteger('id_jabatan');
            $table->timestamps();
            $table->foreign('id_warga')->references('id')->on('wargas')->onDelete('cascade');
            $table->foreign('id_jabatan')->references('id')->on('jabatans')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('wargajabatan');
    }
}
