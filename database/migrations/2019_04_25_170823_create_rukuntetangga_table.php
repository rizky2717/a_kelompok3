<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRukuntetanggaTable extends Migration
{
    public function up()
    {
        Schema::create('rukuntetanggas', function (Blueprint $table) {
            $table->increments('id_rt');
            $table->integer('rt');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('rukuntetangga');
    }
}
