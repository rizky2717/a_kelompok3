<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\warga;

class WargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nwarga = warga::paginate(20);
        return view('warga.index', compact('nwarga'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('warga.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $warga_baru = NEW warga();
        $warga_baru->nama_lengkap = $request->get('nama_lengkap');
        $warga_baru->nik = $request->get('nik');
        $warga_baru->jenis_kelamin = $request->get('jenis_kelamin');
        $warga_baru->tempat_lahir = $request->get('tempat_lahir');
        $warga_baru->tanggal_lahir = $request->get('tanggal_lahir');
        $warga_baru->agama = $request->get('agama');
        $warga_baru->pendidikan = $request->get('pendidikan');
        $warga_baru->jenis_pekerjaan = $request->get('jenis_pekerjaan');
        $warga_baru->status_perkawinan = $request->get('status_perkawinan');
        $warga_baru->status_hubungan = $request->get('status_hubungan');
        $warga_baru->kewarganegaraan = $request->get('kewarganegaraan');
        $warga_baru->no_kk = $request->get('no_kk');
        $warga_baru->save();

        return redirect()->route('data-penduduk.index')->with('status','Warga baru berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($nik)
    {
        // $warga = warga::findOrFail($nik);
        $warga = warga::where('nik', $nik)->firstOrFail();
        return view('warga.show',['warga'=>$warga]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($nik)
    {
    //   $warga = warga::findOrFail($id);
      $warga = warga::where('nik', $nik)->firstOrFail();
      return view('warga.edit',['warga'=>$warga]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $nik)
    {
        $ubah_warga = warga::where('nik', $nik)->firstOrFail();
        $ubah_warga->nama_lengkap = $request->get('nama_lengkap');
        $ubah_warga->nik = $request->get('nik');
        $ubah_warga->jenis_kelamin = $request->get('jenis_kelamin');
        $ubah_warga->tempat_lahir = $request->get('tempat_lahir');
        $ubah_warga->tanggal_lahir = $request->get('tanggal_lahir');
        $ubah_warga->agama = $request->get('agama');
        $ubah_warga->pendidikan = $request->get('pendidikan');
        $ubah_warga->jenis_pekerjaan = $request->get('jenis_pekerjaan');
        $ubah_warga->status_perkawinan = $request->get('status_perkawinan');
        $ubah_warga->status_hubungan = $request->get('status_hubungan');
        $ubah_warga->kewarganegaraan = $request->get('kewarganegaraan');
        $ubah_warga->no_kk = $request->get('no_kk');
        $ubah_warga->save();

        return redirect()->route('data-penduduk.index')->with('status','Warga berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nik)
    {
        $warga = warga::where('nik', $nik)->firstOrFail();

        $warga->delete();
        return redirect()->route('data-penduduk.index')->with('status', 'Warga Berhasil Dihapus');
    }
}
