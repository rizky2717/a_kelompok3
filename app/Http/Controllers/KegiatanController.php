<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kegiatan;

class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $nkegiatan = kegiatan::paginate(20);
        return view('kegiatan.index', compact('nkegiatan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kegiatan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $kegiatan_baru = NEW kegiatan();
        $kegiatan_baru->nama_kegiatan = $request->get('nama_kegiatan');
        $kegiatan_baru->jenis_kegiatan = $request->get('jenis_kegiatan');
        $kegiatan_baru->tanggal = $request->get('tanggal');
        $kegiatan_baru->deskripsi = $request->get('deskripsi');



        $kegiatan_baru->save();

        return redirect()->route('kegiatan-desa.index')->with('status','Kegiatan baru berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_kegiatan)
    {
        $kegiatan = kegiatan::find($id_kegiatan);
        return view('kegiatan.show',['kegiatan'=>$kegiatan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_kegiatan)
    {
        $kegiatan = kegiatan::find($id_kegiatan);
        return view('kegiatan.edit',['kegiatan'=>$kegiatan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_kegiatan)
    {

        $ubah_kegiatan = kegiatan::findOrFail($id_kegiatan);
        $ubah_kegiatan->nama_kegiatan = $request->get('nama_kegiatan');
        $ubah_kegiatan->jenis_kegiatan = $request->get('jenis_kegiatan');
        $ubah_kegiatan->tanggal = $request->get('tanggal');
        $ubah_kegiatan->deskripsi = $request->get('deskripsi');

        $ubah_kegiatan->save();

        return redirect()->route('kegiatan-desa.index')->with('status','Kegiatan berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_kegiatan)
    {
        $kegiatan = kegiatan::where('id_kegiatan', $id_kegiatan)->firstOrFail();

        $kegiatan->delete();
        return redirect()->route('kegiatan-desa.index')->with('status', 'kegiatan Berhasil Dihapus');
    }
}
