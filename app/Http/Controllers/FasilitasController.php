<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\fasilitas;

class FasilitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nfasilitas = fasilitas::paginate(20);
        return view('fasilitas.index', compact('nfasilitas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fasilitas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fasilitas_baru = new fasilitas;
        $fasilitas_baru->nama_fasilitas = $request->get('nama_fasilitas');
        $fasilitas_baru->lokasi = $request->get('lokasi');
        $fasilitas_baru->deskripsi = $request->get('deskripsi');
        $fasilitas_baru->save();
        return redirect()->route('fasilitas-desa.index')->with('status', 'Fasilitas Berhasil Dibuat');
        // echo "anda berada pada method tambah";

        // return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fasilitas = fasilitas::findOrFail($id);
        return view('fasilitas.show', ['fasilitas'=>$fasilitas]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fasilitas = fasilitas::findOrFail($id);
        return view('fasilitas.edit', ['fasilitas'=>$fasilitas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fasilitas_baru = fasilitas::findOrFail($id);
        $fasilitas_baru->nama_fasilitas = $request->get('nama_fasilitas');
        $fasilitas_baru->lokasi = $request->get('lokasi');
        $fasilitas_baru->deskripsi = $request->get('deskripsi');
        $fasilitas_baru->save();
        return redirect()->route('fasilitas-desa.index')->with('status', 'Fasilitas Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fasilitas = fasilitas::findOrFail($id);
        $fasilitas->delete();

        return redirect()->route('fasilitas-desa.index')->with('status', 'Fasilitas Berhasil Dihapus');
    }
}
