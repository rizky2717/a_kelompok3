<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rukuntetangga extends Model
{
    use Notifiable;

    protected $primaryKey = 'id_rt';

    protected $fillable = [
        'id_rt', 'rt'
    ];

    public function kartukeluarga() {
        return $this->hasMany('App\kartukeluarga');
    }
}
