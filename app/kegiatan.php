<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kegiatan extends Model
{
    //use Notifiable;

    protected $primaryKey = 'id_kegiatan';

    protected $fillable = [
        'nama_kegiatan', 'jenis_kegiatan', 'jenis_kegiatan', 'tanggal', 'deskripsi'
    ];

    public function galeri() {
        return $this->hasMany('App\galeri');
    }
}
