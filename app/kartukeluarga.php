<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kartukeluarga extends Model
{
    use Notifiable;

    protected $primaryKey = 'id_kk';

    protected $fillable = [
        'no_kk', 'id_rt'
    ];

    public function rukuntetangga() {
        return $this->belongsTo('App\rukuntetangga');
    }
}
