<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class warga extends Model
{
    use Notifiable;

    protected $fillable = [
        'id',
        'nama_lengkap', 'nik', 'jenis_kelamin', 
        'tempat_lahir',
        'tanggal_lahir', 'agama', 'pendidikan', 
        'jenis_pekerjaan',
        'status_perkawinan', 'status_hubungan', 
        'kewarganegaraan', 'no_kk'
    ];

    public function jabatan() {
        return $this->belongsToMany('App\jabatan');
    }
}
