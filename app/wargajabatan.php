<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class wargajabatan extends Model
{
    use Notifiable;

    //protected $table = 'wargajabatan';
    protected $fillable = [
        'id', 'id_warga', 'id_jabatan'
    ];
}
