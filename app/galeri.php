<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class galeri extends Model
{
    use Notifiable;

    protected $primaryKey = 'id_galeri';

    protected $fillable = [
        'lokasi_galeri', 'id_kegiatan'
    ];

    public function kegiatan() {
        return $this->belongsTo('App\kegiatan');
    }
}
