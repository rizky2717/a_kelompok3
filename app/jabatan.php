<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jabatan extends Model
{

    protected $fillable = [
        'id', 'nama_jabatan'
    ];

    public function warga() {
        return $this->belongsToMany('App\warga');
    }
}
