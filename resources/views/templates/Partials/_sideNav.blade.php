<nav class="col-md-2 d-none d-md-block bg-dark sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-link">
                <a class="nav-item active" href="{{route('data-penduduk.index')}}">
                    <span data-feather="user"></span>
                    Warga
                </a>
            </li>
        </ul>

        <ul class="nav flex-column">
            <li class="nav-link">
                <a class="nav-item active" href="{{route('fasilitas-desa.index')}}">
                    <span data-feather="list"></span>
                    Fasilitas
                </a>
            </li>
        </ul>

        <ul class="nav flex-column">
            <li class="nav-link">
                <a class="nav-item active" href="{{route('kegiatan.index')}}">
                    <span data-feather="book"></span>
                    Kegiatan
                </a>
            </li>
        </ul>
    </div>
</nav>
