<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<title>Desa Purwajaya</title>
{{-- <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}"> --}}
    <meta name="viewport" content="width=device-width, initial-scale=1">

        <style>

            * {box-sizing: border-box;margin: 0;}
            body {
                font-family: Verdana, sans-serif;
                margin:0;
                background-image: url('kantordesa.jpg');
                background-size: 100% 100%;
                background-attachment:fixed;
            }
            .mySlides {display: none}

            img {
                vertical-align: middle;
                height: 520px;
            }

            /* Slideshow container */
            .slideshow-container {
              /* border: 1px solid black; */
                max-width: 900px;
              position: relative;
              margin: auto;
            }

            #informasi {
                position: relative;
                margin-top: 7%;
                z-index: 1;
            }
            /* Next & previous buttons */
            .prev, .next {
              cursor: pointer;
              position: absolute;
              top: 50%;
              width: auto;
              padding: 16px;
              margin-top: -22px;
              color: white;
              font-weight: bold;
              font-size: 18px;
              transition: 0.6s ease;
              border-radius: 0 3px 3px 0;
              user-select: none;
            }


            /* Position the "next button" to the right */
            .next {
              right: 0;
              border-radius: 3px 0 0 3px;
            }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover, .next:hover {
              background-color: rgba(0,0,0,0.8);
            }


            /* The dots/bullets/indicators */
            .dot {
              cursor: pointer;
              height: 15px;
              width: 15px;
              margin: 0 2px;
              background-color: #bbb;
              border-radius: 50%;
              display: inline-block;
              transition: background-color 0.6s ease;
            }

            .active, .dot:hover {
              background-color: #717171;
            }

            /* Fading animation */
            .fade {

              -webkit-animation-name: fade;
              -webkit-animation-duration: 1.5s;
              animation-name: fade;
              animation-duration: 1.5s;
            }

            @-webkit-keyframes fade {
              from {opacity: .4}
              to {opacity: 1}
            }

            @keyframes fade {
              from {opacity: .4}
              to {opacity: 1}
            }

            /* On smaller screens, decrease text size */
            @media only screen and (max-width: 300px) {
              .prev, .next,.text {font-size: 11px}
            }

            .header {
                width: 100%;
                padding: 10px 20px;
                display: inline-block;
                height: 94px;
                position: fixed;
                margin-top: -105px;
                background-color: white;
                z-index: 2;
            }


            .header *{
                width: auto;
                display: inline-block;
            }

            .menu {
                font-size: 18px;
                font-weight: bold;
                position: fixed;
                right: 0;
                text-decoration: none;
                margin: 10px 20px;
            }

            .menu ul li{
                margin: 10px;
            }

            .logo {
                margin-top: -30px;
            }

            .inline-block{
                display: inline-block;
            }

            #fasilitas {
                background-color: rgba(255,255,204,1);
                width: 100%;
                min-height: 100%;
                padding: 20px;
            }

            #fasilitas * {
                text-align: center;
                padding: 10px 30px;
                /* border: 1px solid salmon; */
            }

            #kegiatan{

                background-color: rgba(153,204,102,1);
                min-height: 100%;
                padding: 20px;
            }


            #kegiatan * {
                text-align: center;
                padding: 10px 30px;
            }

            #visimisi{
                min-height: 100%;
                padding: 30px 150px;
            }

            #visimisi * {
                padding: 10px 30px;
            }

            #kontak{

                background-color:rgba(255,233,0,0.5);
            }

            .konten {
                background-color: rgba(64,64,64,0.75);
            }

            .kata * {
                display: block;
            }

            .menu a {
                color: green;
            }
            .menu a:hover {
                color: salmon;
            }

            .section-header {
                text-align:center;
                font-size: 34px;
                font-weight: bold;
            }

            #footer {
                background-color: #191922;
                padding: 25px 0;
                color: #eee;
                text-align: center;
            }

            .inline-block img {
                height: auto;
                width: 220px;
            }

            #fasilitas span {
                font-weight: bold;
                font-size: 24px;
            }

            #kegiatan * {
                text-align: center;
            }
        </style>
    </head>
<body>
{{-- <div class="menu"> --}}
<div class="konten">
    <div class="header">
        <div class="logo"><img src="logokukar.png" alt="" style="height: 80px; width: 65px;  margin-top: -30px;"></div>
        <div class="kata"><span style="font-size: 30px; font-weight:bold;">Desa Purwajaya</span><span style="font-size: 16px; font-weight:bold;">Kabupaten Kutai Kartanegara</span></div>
        <div class="menu">
            <ul>
                <a href="#informasi">
                    <li>Informasi</li>
                </a>
                <a href="#fasilitas">
                    <li>Fasilitas</li>
                </a>
                <a href="#kegiatan">
                    <li>Kegiatan</li>
                </a>
                <a href="#visimisi">
                    <li>Visi Misi</li>
                </a>
                <a href="#kontak">
                    <li>Kontak Kami</li>
                </a>

            </ul>
        </div>
    </div>
{{-- </div> --}}
    <div id="informasi">
        <div class="slideshow-container">

            <div class="mySlides fade">

                <img src="perempuan-pendidikan-2.jpg" style="width:100%">

            </div>

            <div class="mySlides fade">

                <img src="lapangann.jpg" style="width:100%">

            </div>

            <div class="mySlides fade">

                <img src="84483-desa_12.jpg" style="width:100%">

            </div>

            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>

        </div>
        <br>

        <div style="text-align:center">
            <span class="dot" onclick="currentSlide(1)"></span>
            <span class="dot" onclick="currentSlide(2)"></span>
            <span class="dot" onclick="currentSlide(3)"></span>
        </div>

        <script>
            var slideIndex = 1;
            showSlides(slideIndex);

            function plusSlides(n) {
                showSlides(slideIndex += n);
            }

            function currentSlide(n) {
                showSlides(slideIndex = n);
            }

            function showSlides(n) {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("dot");
                if (n > slides.length) {slideIndex = 1}
                if (n < 1) {slideIndex = slides.length}
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex-1].style.display = "block";
                dots[slideIndex-1].className += " active";
            }
            </script>



        </div>



        <div id="fasilitas">
            <div class="section-header">
                <span>FASILITAS</span>
            </div>
            <div class="row">
                    {{-- @for ($i = 0; $i < 8; $i++) --}}
                <div class="inline-block">
                    <div style="border-radius: 70px;">
                        <img src="{{ asset('fasilitas/lapangan.png') }}" alt="">
                    </div>
                    <div style="padding: 20px 0;">
                        <span>LAPANGAN</span>
                   </div>
                </div>

                <div class="inline-block">
                    <div style="border-radius: 70px;">
                        <img src="{{ asset('fasilitas/medis.png') }}" alt="">
                    </div>
                    <div style="padding: 20px 0;">
                        <span>FASILITAS</span>
                    </div>
                </div>

                <div class="inline-block">
                    <div style="border-radius: 70px;">
                        <img src="{{ asset('fasilitas/belanja.png') }}" alt="">
                    </div>
                    <div style="padding: 20px 0;">
                        <span>MINIMARKET</span>
                    </div>
                </div>

                <div class="inline-block">
                    <div style="border-radius: 70px;">
                        <img src="{{ asset('fasilitas/meeting.png') }}" alt="" style="width: 240px;">
                    </div>
                    <div style="padding: 20px 0;">
                        <span>GEDUNG PERTEMUAN</span>
                    </div>
                </div>

                <div class="inline-block">
                    <div style=" border-radius: 70px;">
                        <img src="{{ asset('fasilitas/perpustakaan.png') }}" alt="" style="width: 400px; margin-top: -20px;">
                    </div>
                    <div style="padding: 20px 0;">
                        <span>PERPUSTAKAAN</span>
                    </div>
                </div>

                <div class="inline-block">
                    <div style=" border-radius: 70px;">
                        <img src="{{ asset('fasilitas/renang.png') }}" alt="">
                    </div>
                    <div style="padding: 20px 0;">
                        <span>WAHANA</span>
                    </div>
                </div>

                <div class="inline-block">
                    <div style="border-radius: 70px;">
                        <img src="{{ asset('fasilitas/rip makam.png') }}" alt="" style="width: 240px;">
                    </div>
                    <div style="padding: 20px 0;">
                        <span>PEMAKAMAN</span>
                    </div>
                </div>

                <div class="inline-block">
                    <div style="border-radius: 70px;">
                        <img src="{{ asset('fasilitas/taman.png') }}" alt="" style="width: 270px;">
                    </div>
                    <div style="padding: 20px 0;">
                        <span>TAMAN</span>
                    </div>
                </div>
                    {{-- @endfor --}}
            </div>
            {{-- @foreach ($nfasilitas as $fasilitas)
                <div></div>
            @endforeach --}}
        </div>
        <div id="kegiatan">
            <div class="section-header">
                <span>KEGIATAN</span>
            </div>
            <div class="row" >
                    <div class="inline-block">
                        <div style="width: 350px; height: 350px;   ">
                        <img src="{{ asset('kegiatan/17agt45.jpg') }}" style="width: 400px; border-radius: 70px;">
                        </div>
                        <div style="padding: 20px 0;">
                            Lomba gerak jalan <br>untuk memperingati hari kemerdekaan
                        </div>
                    </div>

                    <div class="inline-block">
                        <div style="width: 350px; height: 350px;   border-radius: 70px;">
                        <img src="{{ asset('kegiatan/irigasi.jpg') }}" style="width: 400px; border-radius: 70px;">
                        </div>
                        <div style="padding: 20px 0;">
                            pembuatan irigasi sawah di rt 2
                        </div>
                    </div>

                    <div class="inline-block">
                        <div style="width: 350px; height: 350px;   border-radius: 70px;">
                        <img src="{{ asset('kegiatan/projekjalan.jpg') }}" style="width: 400px; border-radius: 70px;" >
                        </div>
                        <div style="padding: 20px 0;">
                            pengaspalan jalan <br>sepanjang 1 km di Jln.Rambutan
                        </div>
                    </div>
                    {{-- @for ($i = 0; $i < 3; $i++) --}}

                    {{-- @endfor --}}
            </div>
            <div>
                <a href=""><button type="submit" value="SELENGKAPNYA">SELENGKAPNYA</button></a>
            </div>
        </div>
        <div id="visimisi">
            <div class="section-header" style="color:gold;">
                <span>VISI MISI</span>
            </div>
            <div class="row" style="color: white;">
                <ol>
                    <li>Meningkatkan perekonomian masyarakat melalui pengembangan pola pertanian, perkebunan, peternakan, perikanan dan perdagangan sesuai dengan usaha masyarakat dan potensi desa.</li>
                    <li>Meningkatkan sumber daya manusia melalui pembinaan dan pelatihan kelompok dan lembaga kemasyarkatan</li>
                    <li>Meningkatkan pembangunan insfrastruktur yang berkelanjutan</li>
                    <li>Meningkatkan kesehatan dan mutu pendidikan masyrakat dari usia dini</li>
                    <li>Meningkatkan kehidupan sosial, seni budaya, pemuda dan olah raga dalam bingkai kearifan lokal</li>
                    <li>Membangun kemandirian desa dengan menggali dan mengembangkan potensi dan usaha desa</li>
                    <li>Menciptakan Gunungrejo yang sejuk, nyaman, indah, asri dan bersahaja</li>
                </ol>
            </div>
        </div>
        <div id="kontak">

       </div>

    </div>

    <div id="footer">
        <div class="container">

                    <div class='footer-copyleft'>
                    Kelompok3&copy; 2019</div>
                    <div>Content by &reg; Desa Purwajaya, Kab. Kutai Kartanegara<br /></div>
                    082187665019

        </div>
    </div>
    </body>
</html>
