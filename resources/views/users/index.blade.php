<html>
<head>
    <title>User</title>
    <link rel="stylesheet" type="text/css" href="{{asset("bootstrap/css/bootstrap.min.css")}}">
    <style>
        table
        {
            boder-collapse: collapse;
            width: 30%;
        }
        table, td, th
        {
            border: 2.5px solid black;
        }
        #center
        {
            text-align:center;
        }
    </style>
</head>
<body>
    <div class="well">
    <h1>Data Users</h1>
    <table class="table table-striped">
        <thead>
            <tr class="table-primary">
                <th id="center">ID</th>
                <th id="center">NAMA</th>
                <th id="center">NIM</th>
                <th id="center">KELAS</th>
            </tr>
        </thead>
        @foreach($users as $user)
        <tr>
            <td id="center">{{$user['id']}}</td>
            <td>{{$user['nama']}}</td>
            <td id="center">{{$user['nim']}}</td>
            <td id="center">{{$user['kelas']}}</td>
        </tr>
        @endforeach
    </table>
    <style>
    <div align="right"></div>
    {!! $users->render()!!}
    </style>
<script type="text/javascript" src="{{asset('bootstrap/js/bootstrap.min.js')}}">
</body>
</html>
