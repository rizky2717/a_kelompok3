@extends('templates.home')
@section('title')
	Ubah Data Warga
@endsection
@section('content')

	<div class="container">
		<h1>Ubah Data Warga</h1>
		<hr>

		<div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">

		<div class="card-header bg-primary text-white">
			<h5 class="text-light"> Ubah Data Warga Detail </h5>
		</div>

		<div class="card-body">

		<div class="container text-primary">
            <form action="{{ route('data-penduduk.update', ['nik'=>$warga->nik]) }}" method="POST" class="form-group"
                enctype="multipart/form-data">

                <input name="_method" type="hidden" value="PATCH">
				@csrf
                @method('PUT')

				    <div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="no_kk">NO KK</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="no_kk" value="{{ $warga['no_kk'] }}">
						{{ ($errors->has('no_kk')) ? $errors->first('no_kk') : "" }}
					</div>
					</div>
                    <br>

					<div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="nik">NIK</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nik" value="{{ $warga['nik'] }}">
						{{ ($errors->has('nik')) ? $errors->first('nik') : "" }}
					</div>
					</div>
                    <br>

					<div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="nama_lengkap">Nama Lengkap</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nama_lengkap" value="{{ $warga['nama_lengkap'] }}">
						{{ ($errors->has('nama_lengkap')) ? $errors->first('nama_lengkap') : "" }}
					</div>
					</div>
                    <br>
                    <div class="row">
                    <div class="col-md-3">
                    <label for="jenis_kelamin">Jenis Kelamin</label><br>
                    </div>
                    <input type="radio" name="jenis_kelamin" value="Laki-Laki"><span>Laki-Laki</span>
                    <input type="radio" name="jenis_kelamin" value="Perempuan"><span>Perempuan</span> <br>
                    </div>

                    <br>
					<div class="row">
						<div class="col-md-3">
							<label for="tempat_lahir" class="text-primary">Tempat Lahir</label>
						</div>
						<div class="col-md-8">
                            <input class="form-control" type="text" name="tempat_lahir" value="{{ $warga['tempat_lahir'] }}">{{ ($errors->has('tempat_lahir')) ? $errors->first('tempat_lahir') : "" }}
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<label for="tanggal_lahir" class="text-primary">Tanggal Lahir</label>
						</div>
						<div class="col-md-8">
                            <input class="form-control" type="date" name="tanggal_lahir" value="{{ $warga['tanggal_lahir'] }}">{{ ($errors->has('tempat_lahir')) ? $errors->first('tempat_lahir') : "" }}
						</div>
					</div>
                    <br>
                    <div class="row">
					<div class="col-md-3">
                    <label for="agama">Agama</label>
                    </div>
                    <div class="col-md-8">
                    <select class="" name="agama">
                        <option value="{{ $warga['agama'] }}">{{ $warga['agama'] }}</option>
                        <option value="Islam">Islam</option>
                        <option value="Katholik">Katholik</option>
                        <option value="Protestan">Protestan</option>
                        <option value="Buddha">Buddha</option>
                        <option value="Hindu">Hindu</option>
                        <option value="Konghucu">Konghucu</option>
                    </select>

						</div>
					</div>
                    <br>
                    <div class="row">
                    <div class="col-md-3">
                    <label for="pendidikan">Pendidikan</label>
                    </div>
                    <div class="col-md-8">
                        <select class="" name="pendidikan">
                            <option value="{{ $warga['pendidikan'] }}">{{ $warga['pendidikan'] }}</option>
                            <option value="Tidak/Belum Sekolah">Tidak/Belum Sekolah</option>
                            <option value="Belum Tamat SD/Sederajat">Belum Tamat SD/Sederajat</option>
                            <option value="Tamat SD/Sederajat">Tamat SD/Sederajat</option>
                            <option value="SLTP/Sederajat">SLTP/Sederajat</option>
                            <option value="SLTA/Sederajat">SLTA/Sederajat</option>
                            <option value="Diploma I/II">Diploma I/II</option>
                            <option value="Akademi/Diploma III/Sarjana Muda">Akademi/Diploma III/Sarjana Muda</option>
                            <option value="Diploma IV/Strata I">Diploma IV/Strata I</option>
                            <option value="Strata II">Strata II</option>
                            <option value="Strata III">Strata III</option>
                        </select>
                    </div>
                    </div>
                    <br>
                    <div class="row">
                    <div class="col-md-3">
                    <label for="jenis_pekerjaan">Pekerjaan</label>
                    </div>
                    <div class="col-md-8">
                        <select class="" name="jenis_pekerjaan">
                            <option value="{{ $warga['jenis_pekerjaan'] }}">{{ $warga['jenis_pekerjaan'] }}</option>
                            <option value="Belum/Tidak Bekerja">Tidak/Belum Sekolah</option>
                            <option value="Mengurus Rumah Tangga">Mengurus Rumah Tangga</option>
                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                            <option value="Pensiunan">Pensiunan</option>
                            <option value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
                            <option value="TNI">TNI</option>
                            <option value="Petani/Pekebun">Petani/Pekebun</option>
                            <option value="Peternak">Peternak</option>
                            <option value="Karyawan Swasta">Karyawan Swasta</option>
                            <option value="Buruh Harian Lepas">Buruh Harian Lepas</option>
                            <option value="Buruh Tani/Perkebunan">Buruh Tani/Perkebunan</option>
                            <option value="Pembantu Rumah Tangga">Pembantu Rumah Tangga</option>
                            <option value="Tukang Cukur">Tukang Cukur</option>
                            <option value="Tukang Listrik">Tukang Listrik</option>
                            <option value="Tukang Batu">Tukang Batu</option>
                            <option value="Tukang Kayu">Tukang Kayu</option>
                            <option value="Mekanik">Mekanik</option>
                            <option value="Paraji">Paraji</option>
                            <option value="Imam Masjid">Imam Masjid</option>
                            <option value="Wartawan">Wartawan</option>
                            <option value="Ustadz/Ustadzah/Mubaligh">Ustadz/Ustadzah/Mubaligh</option>
                            <option value="Guru">Guru</option>
                            <option value="Sopir">Sopir</option>
                            <option value="Pedagang">Pedagang</option>
                            <option value="Perangkat Desa">Perangkat Desa</option>
                            <option value="Kepala Desa">Kepala Desa</option>
                            <option value="Wiraswasta">Wiraswasta</option>
                        </select>
                    </div>
                    </div>
                        <br>
                        <div class="row">
                         <div class="col-md-3">
                        <label for="status_perkawinan">Status Perkawinan</label>
                         </div>
                         <div class="col-md-8">
                        <select class="" name="status_perkawinan">
                            <option value="{{ $warga['status_perkawinan'] }}">{{ $warga['status_perkawinan'] }}</option>
                            <option value="Belum Kawin">Belum Kawin</option>
                            <option value="Kawin">Kawin</option>
                            <option value="Cerai Hidup">Cerai Hidup</option>
                            <option value="Cerai Mati">Cerai Mati</option>
                        </select>
                         </div>
                        </div>
                        <br>

                        <div class="row">
                        <div class="col-md-3">
                        <label for="status_hubungan">Status Hubungan</label>
                        </div>
                        <div class="col-md-8">
                        <select class="" name="status_hubungan">
                            <option value="{{ $warga['status_hubungan'] }}">{{ $warga['status_hubungan'] }}</option>
                            <option value="Kepala Keluarga">Kepala Keluarga</option>
                            <option value="Suami">Suami</option>
                            <option value="Istri">Istri</option>
                            <option value="Anak">Anak</option>
                            <option value="Menantu">Menantu</option>
                            <option value="Cucu">Cucu</option>
                            <option value="Orang Tua">Orang Tua</option>
                            <option value="Mertua">Mertua</option>
                            <option value="Pembantu">Pembantu</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>
                        </div>
                        </div>
                        <br>

                        <div class="row">
                                <div class="col-md-3">
                            <label for="kewarganegaraan">Kewarganegaraan</label>
                                </div>
                                <div class="col-md-8">
                            <input type="radio" name="kewarganegaraan" value="WNI"><span>WNI</span>
                            <input type="radio" name="kewarganegaraan" value="WNA"><span>WNA</span>
                                </div>
                        </div>
                            <br>

					<div class="row">
						<div class="col-md-3 offset-md-5 offset-md-4">
							<button type="submit" class="btn btn-outline-primary">Simpan</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
