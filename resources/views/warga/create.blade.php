@extends('templates.home')
@section('title')
	Tambah Data Warga
@endsection
@section('content')

	<div class="container">
		<h1>Tambah Data Warga</h1>
		<hr>

		<div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">

		<div class="card-header bg-primary text-white">
			<h5 class="text-light"> Tambah Data Warga </h5>
		</div>

		<div class="card-body">

		<div class="container text-primary">
                <form class="form-group" action="{{ route('data-penduduk.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                @method('PUT')

				    <div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="no_kk">NO KK</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="no_kk" value="">
						{{ ($errors->has('no_kk')) ? $errors->first('no_kk') : "" }}
					</div>
					</div>
                    <br>

					<div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="nik">NIK</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nik" value="">
						{{ ($errors->has('nik')) ? $errors->first('nik') : "" }}
					</div>
					</div>
                    <br>

					<div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="nama_lengkap">Nama Lengkap</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nama_lengkap" value="">
						{{ ($errors->has('nama_lengkap')) ? $errors->first('nama_lengkap') : "" }}
					</div>
					</div>
                    <br>
                    <div class="row">
                    <div class="col-md-3">
                    <label for="jenis_kelamin">Jenis Kelamin</label><br>
                    </div>
                    <input type="radio" name="jenis_kelamin" value="Laki-Laki"><span>Laki-Laki</span>
                    <input type="radio" name="jenis_kelamin" value=""><span>Perempuan</span> <br>
                    </div>

                    <br>
					<div class="row">
						<div class="col-md-3">
							<label for="tempat_lahir" class="text-primary">Tempat Lahir</label>
						</div>
						<div class="col-md-8">
                            <input class="form-control" type="text" name="tempat_lahir" value="">{{ ($errors->has('tempat_lahir')) ? $errors->first('tempat_lahir') : "" }}
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<label for="tanggal_lahir" class="text-primary">Tanggal Lahir</label>
						</div>
						<div class="col-md-8">
                            <input class="form-control" type="date" name="tanggal_lahir" value="">{{ ($errors->has('tempat_lahir')) ? $errors->first('tempat_lahir') : "" }}
						</div>
					</div>
                    <br>
                    <div class="row">
					<div class="col-md-3">
                    <label for="agama">Agama</label>
                    </div>
                    <div class="col-md-8">
                            <select class="" name="agama">

                                    <option value="">Islam</option>
                                    <option value="">Katholik</option>
                                    <option value="">Protestan</option>
                                    <option value="">Buddha</option>
                                    <option value="">Hindu</option>
                                    <option value="">Konghucu</option>
                                </select>

						</div>
					</div>
                    <br>
                    <div class="row">
                    <div class="col-md-3">
                    <label for="pendidikan">Pendidikan</label>
                    </div>
                    <div class="col-md-8">
                        <select class="" name="pendidikan">

                            <option value="">Tidak/Belum Sekolah</option>
                            <option value="">Belum Tamat SD/Sederajat</option>
                            <option value="">Tamat SD/Sederajat</option>
                            <option value="">SLTP/Sederajat</option>
                            <option value="">SLTA/Sederajat</option>
                            <option value="">Diploma I/II</option>
                            <option value="">Akademi/Diploma III/Sarjana Muda</option>
                            <option value="">Diploma IV/Strata I</option>
                            <option value="">Strata II</option>
                            <option value="">Strata III</option>
                        </select>
                    </div>
                    </div>
                    <br>
                    <div class="row">
                    <div class="col-md-3">
                    <label for="jenis_pekerjaan">Pekerjaan</label>
                    </div>
                    <div class="col-md-8">
                        <select class="" name="jenis_pekerjaan">

                            <option value="">Tidak/Belum Sekolah</option>
                            <option value="">Mengurus Rumah Tangga</option>
                            <option value="">Pelajar/Mahasiswa</option>
                            <option value="">Pensiunan</option>
                            <option value="">Pegawai Negeri Sipil</option>
                            <option value="">TNI</option>
                            <option value="">Petani/Pekebun</option>
                            <option value="">Peternak</option>
                            <option value="">Karyawan Swasta</option>
                            <option value="">Buruh Harian Lepas</option>
                            <option value="">Buruh Tani/Perkebunan</option>
                            <option value="">Pembantu Rumah Tangga</option>
                            <option value="">Tukang Cukur</option>
                            <option value="">Tukang Listrik</option>
                            <option value="">Tukang Batu</option>
                            <option value="">Tukang Kayu</option>
                            <option value="">Mekanik</option>
                            <option value="">Paraji</option>
                            <option value="">Imam Masjid</option>
                            <option value="">Wartawan</option>
                            <option value="">Ustadz/Ustadzah/Mubaligh</option>
                            <option value="">Guru</option>
                            <option value="">Sopir</option>
                            <option value="">Pedagang</option>
                            <option value="">Perangkat Desa</option>
                            <option value="">Kepala Desa</option>
                            <option value="">Wiraswasta</option>
                        </select>
                    </div>
                    </div>
                        <br>
                        <div class="row">
                         <div class="col-md-3">
                        <label for="status_perkawinan">Status Perkawinan</label>
                         </div>
                         <div class="col-md-8">
                        <select class="" name="status_perkawinan">

                            <option value="">Belum Kawin</option>
                            <option value="">Kawin</option>
                            <option value="">Cerai Hidup</option>
                            <option value="">Cerai Mati</option>
                        </select>
                         </div>
                        </div>
                        <br>

                        <div class="row">
                        <div class="col-md-3">
                        <label for="status_hubungan">Status Hubungan</label>
                        </div>
                        <div class="col-md-8">
                        <select class="" name="status_hubungan">

                            <option value="">Kepala Keluarga</option>
                            <option value="">Suami</option>
                            <option value="">Istri</option>
                            <option value="">Anak</option>
                            <option value="">Menantu</option>
                            <option value="">Cucu</option>
                            <option value="">Orang Tua</option>
                            <option value="">Mertua</option>
                            <option value="">Pembantu</option>
                            <option value="">Lainnya</option>
                        </select>
                        </div>
                        </div>
                        <br>

                        <div class="row">
                                <div class="col-md-3">
                            <label for="kewarganegaraan">Kewarganegaraan</label>
                                </div>
                                <div class="col-md-8">
                            <input type="radio" name="kewarganegaraan" value=""><span>WNI</span>
                            <input type="radio" name="kewarganegaraan" value=""><span>WNA</span>
                                </div>
                        </div>
                            <br>

					<div class="row">
						<div class="col-md-3 offset-md-5 offset-md-4">
							<button type="submit" class="btn btn-outline-primary">Simpan</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
