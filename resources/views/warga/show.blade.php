
@extends('templates.home')
@section('title')
	Detail Warga
@endsection
@section('content')

	<h1>Detail Warga</h1>
	<hr>
	<br>

	<div class="card bg-white border-info" style="max-width: 70%; margin: auto; min-height: 400px;">


		<div class="row">
			<div class="col-md-12 text-center">
				<h3>NIK = {{ $warga->nik }} </h3>
			</div>
		</div>
		<hr>
		<br>
		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				No KK
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $warga->no_kk }}
			</div>
			<br>
		</div>


		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Nama Lengkap
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $warga->nama_lengkap }}
			</div>
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Tanggal Lahir
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $warga->tanggal_lahir }}
			</div>
        </div>

        <div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Agama
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $warga->agama }}
			</div>
        </div>

        <div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Pendidikan
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $warga->pendidikan }}
			</div>
        </div>

        <div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Status Perkawinan
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $warga->status_perkawinan }}
			</div>
        </div>

        <div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Status Hubungan
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $warga->status_hubungan }}
			</div>
        </div>

        <div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Kewarganegaraan
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $warga->kewarganegaraan }}
			</div>
		</div>



	</div>

@endsection

