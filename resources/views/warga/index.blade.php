@extends('templates.home')
@section('title')
	Data warga
@endsection
@section('css')
	<style>
		body
        {
			padding-top: 30px;
		}

		th, td
        {
			padding: 10px;
			text-align: center;
		}

		td a
        {
			margin: 3px;
			align-content: center;
			color: white;
		}

		td a:hover
        {
			text-decoration: none;
		}

		td button
        {
			margin-top: 5px;
			cursor: pointer;
		}
	</style>
@endsection
@section('content')

<div class="container">
    <h1 class="text-light">Data Warga</h1>
    <div class="row">
        <div class="col-md-2">
            <a class="btn btn-outline-primary bg-light" href="{{ route('data-penduduk.create') }}">
                <span data-feather="plus-circle"></span> Tambah <span class="sr-only">(current)</span></a>
        </div>
    </div>
    <br>
    <div class="table-responsive">
    <table class="table table-striped">
    <thead>
        <tr class="bg-primary text-light">

            <th scope="col">No</th>
            <th scope="col">KK</th>
            <th scope="col">NIK</th>
            <th scope="col">Nama</th>
            <th scope="col">Tanggal Lahir</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($nwarga as $warga)
    <tr class="table-light">
        <td>{{ $warga['id'] }}</td>
        <td>{{ $warga['no_kk'] }}</td>
        <td>{{ $warga['nik'] }}</td>
        <td>{{ $warga['nama_lengkap'] }}</td>
        <td>{{ $warga['tanggal_lahir'] }}</td>

        <td class="table-light">
            <a class="btn-sm btn-primary" href="{{ route('data-penduduk.show', ['nik'=>$warga->nik]) }}">
                <span data-feather="eye"></span>Detail<span class="sr-only">(current)</span></a>
            <a class="btn-sm btn-success d-inline" href="{{ route('data-penduduk.edit', ['nik'=>$warga->nik]) }}">
                <span data-feather="edit-2"></span>Edit<span class="sr-only">(current)</span></a>

            <form class="d-inline" onsubmit="return confirm('DELETE this warga permanetly?')"
            action="{{ route('data-penduduk.destroy', ['nik'=>$warga->nik]) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn-sm btn-danger" value="Delete" name="submit">
                    <span data-feather="trash"></span>Delete<span class="sr-only">(current)</span></button>
            </form>

        </td>
    </tr>
    @endforeach
    </tbody>
</table>

</div>
</div>
@endsection
