
@extends('templates.home')
@section('title')
	Kegiatan
@endsection
@section('css')
	<style>
		body
        {
			padding-top: 30px;
		}

		th, td
        {
			padding: 10px;
			text-align: center;
		}

		td a
        {
			margin: 3px;
			align-content: center;
			color: white;
		}

		td a:hover
        {
			text-decoration: none;
		}

		td button
        {
			margin-top: 5px;
			cursor: pointer;
		}
	</style>
@endsection
@section('content')

<div class="container">
    <h1 class="text-light">Kegiatan</h1>
    <div class="row">
        <div class="col-md-2">
            <a class="btn btn-outline-primary bg-light" href="{{ route('kegiatan.create') }}">
                <span data-feather="plus-circle"></span> Tambah <span class="sr-only">(current)</span></a>
        </div>
    </div>
    <br>
    <div class="table-responsive">
    <table class="table table-striped">
    <thead>
        <tr class="bg-primary text-light">
                <th scope="col">Nama Kegiatan</th>
                <th scope="col">Jenis Kegiatan</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
            @foreach ($nkegiatan as $kegiatan)
            <tr class="table-light">

                <td>{{ $kegiatan['nama_kegiatan'] }}</td>
                <td>{{ $kegiatan['jenis_kegiatan'] }}</td>
                <td>{{ $kegiatan['tanggal'] }}</td>
                <td>{{ $kegiatan['deskripsi'] }}</td>
                <td>
                        <a class="btn-sm btn-primary" href="{{ route('kegiatan.show', ['id_kegiatan'=>$kegiatan->id_kegiatan]) }}"><span data-feather="eye"></span>Detail<span class="sr-only">(current)</span></a>
                        <a class="btn-sm btn-success d-inline" href="{{ route('kegiatan.edit', ['id_kegiatan'=>$kegiatan->id_kegiatan]) }}"><span data-feather="edit-2"></span>Edit<span class="sr-only">(current)</span></a>

                        <form class="d-inline" onsubmit="return confirm('DELETE this user permanetly?')" action="{{ route('kegiatan.destroy', ['id_kegiatan'=>$kegiatan->id_kegiatan]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn-sm btn-danger" value="Delete" name="submit"><span data-feather="trash"></span>Delete<span class="sr-only">(current)</span></button>
                        </form>
                    </td>
            </tr>
        @endforeach
    </tbody>
    </table>

</div>
</div>
@endsection

