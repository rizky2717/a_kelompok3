<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>EDIT KEGIATAN</h1>
    <form class="form-group" action="{{ route('kegiatan.update', $kegiatan['id_kegiatan']) }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PATCH">
        @csrf
        <label for="nama_kegiatan">Nama Kegiatan</label>
        <input type="text" name="nama_kegiatan" value="{{ $kegiatan['nama_kegiatan'] }}"><br>

        <label for="jenis_kegiatan">Jenis Kegiatan</label>
        <input type="text" name="jenis_kegiatan" value="{{ $kegiatan['jenis_kegiatan'] }}"><br>

        <label for="tanggal">Tanggal</label>
        <input type="date" name="tanggal" value="{{ $kegiatan['tanggal'] }}"><br>

        <label for="deskripsi">Deskripsi</label>
        <input type="text" name="deskripsi" value="{{ $kegiatan['deskripsi'] }}"><br>
        <button name="button">Submit</button>
    </form>
  </body>
</html>
