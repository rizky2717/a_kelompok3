
@extends('templates.home')
@section('title')
	Detail Fasilitas
@endsection
@section('content')

	<h1>Detail Warga</h1>
	<hr>
	<br>

	<div class="card bg-white border-info" style="max-width: 70%; margin: auto; min-height: 400px;">


		<div class="row">
			<div class="col-md-12 text-center">
				<h3>No = {{ $fasilitas->id_fasilitas }} </h3>
			</div>
		</div>
		<hr>
		<br>
		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Nama Fasilitas
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $fasilitas->nama_fasilitas }}
			</div>
			<br>
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Lokasi
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $fasilitas->lokasi }}
			</div>
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Deskripsi
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $fasilitas->deskripsi }}
			</div>
        </div>
	</div>

@endsection
