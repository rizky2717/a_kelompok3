@extends('templates.home')
@section('title')
	Fasilitas
@endsection
@section('css')
	<style>
		body
        {
			padding-top: 30px;
		}

		th, td
        {
			padding: 10px;
			text-align: center;
		}

		td a
        {
			margin: 3px;
			align-content: center;
			color: white;
		}

		td a:hover
        {
			text-decoration: none;
		}

		td button
        {
			margin-top: 5px;
			cursor: pointer;
		}
	</style>
@endsection
@section('content')

<div class="container">
    <h1 class="text-light">Fasilitas</h1>
    <div class="row">
        <div class="col-md-2">
            <a class="btn btn-outline-primary bg-light" href="{{ route('fasilitas-desa.create') }}">
                <span data-feather="plus-circle"></span> Tambah <span class="sr-only">(current)</span></a>
        </div>
    </div>
    <br>
    <div class="table-responsive">
    <table class="table table-striped">
    <thead>
        <tr class="bg-primary text-light">
            <th scope="col">No</th>
            <th scope="col">Nama Fasilitas</th>
            <th scope="col">Lokasi</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
            @foreach ($nfasilitas as $fasilitas)
            <tr class="table-light">
                <td>{{ $fasilitas['id_fasilitas'] }}</td>
                <td>{{ $fasilitas['nama_fasilitas'] }}</td>
                <td>{{ $fasilitas['lokasi'] }}</td>
                <td>{{ $fasilitas['deskripsi'] }}</td>
                <td class="table-light">
                    <a class="btn-sm btn-primary" href="{{ route('fasilitas-desa.show',$fasilitas['id_fasilitas']) }}">
                        <span data-feather="eye"></span>Detail<span class="sr-only">(current)</span></a>
                    <a class="btn-sm btn-success d-inline" href="{{ route('fasilitas-desa.edit',$fasilitas['id_fasilitas']) }}">
                        <span data-feather="edit-2"></span>Edit<span class="sr-only">(current)</span></a>
                    <form class="d-inline" onsubmit="return confirm('Delete this facility permanently?')"
                        action="{{route('fasilitas-desa.destroy', $fasilitas['id_fasilitas'])}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn-sm btn-danger" value="Delete" name="submit">
                        <span data-feather="trash"></span>Delete<span class="sr-only">(current)</span></button>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>

</div>
</div>
@endsection
