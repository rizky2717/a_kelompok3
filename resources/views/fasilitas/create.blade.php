@extends('templates.home')
@section('title')
	Tambah Data Warga
@endsection
@section('content')

	<div class="container">
		<h1>Tambah Data Warga</h1>
		<hr>

		<div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">

		<div class="card-header bg-primary text-white">
			<h5 class="text-light"> Tambah Data Warga </h5>
		</div>

		<div class="card-body">

		<div class="container text-primary">
                <form class="form-group" action="{{ route('fasilitas-desa.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

					<div class="row">
                        <div class="col-md-3">
                            <label class="text-primary" for="nama_fasilitas">Nama Fasilitas</label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="nama_fasilitas" value="">
                            {{ ($errors->has('nama_fasilitas')) ? $errors->first('nama_fasilitas') : "" }}
                        </div>
					</div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <label class="text-primary" for="lokasi">Lokasi</label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="lokasi" value="">
                            {{ ($errors->has('lokasi')) ? $errors->first('lokasi') : "" }}
                        </div>
                    </div>
                    <br>
					<div class="row">
                        <div class="col-md-3">
                            <label class="text-primary" for="deskripsi">Deskripsi</label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="deskripsi" value="">
                            {{ ($errors->has('deskripsi')) ? $errors->first('deskripsi') : "" }}
                        </div>
						</div>
                    </div>
                    <br>
					<div class="row">
						<div class="col-md-3 offset-md-5 offset-md-4">
							<button type="submit" class="btn btn-outline-primary">Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
