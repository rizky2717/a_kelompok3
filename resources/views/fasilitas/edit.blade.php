@extends('templates.home')
@section('title')
	Ubah Data Warga
@endsection
@section('content')

	<div class="container">
		<h1>Ubah Data Fasilitas</h1>
		<hr>

		<div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">

		<div class="card-header bg-primary text-white">
			<h5 class="text-light"> Ubah Data Fasilitas Detail </h5>
		</div>

		<div class="card-body">

		<div class="container text-primary">
            <form action="{{ route('fasilitas-desa.update', ['id_fasilitas'=>$fasilitas->id_fasilitas]) }}" method="POST" class="form-group"
                enctype="multipart/form-data">

                <input name="_method" type="hidden" value="PATCH">
				@csrf
                @method('PUT')

		</div>
            <br>
					<div class="row">
                        <div class="col-md-3">
                            <label class="text-primary" for="nama_fasilitas">Nama Fasilitas</label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="nama_fasilitas" value="{{ $fasilitas['nama_fasilitas'] }}">
                            {{ ($errors->has('nama_fasilitas')) ? $errors->first('nama_fasilitas') : "" }}
                        </div>
					</div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <label class="text-primary" for="lokasi">Lokasi</label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="lokasi" value="{{ $fasilitas['lokasi'] }}">
                            {{ ($errors->has('lokasi')) ? $errors->first('lokasi') : "" }}
                        </div>
                    </div>
                    <br>
                    <div class="row">
                            <div class="col-md-3">
                                <label class="text-primary" for="deskripsi">Deskripsi</label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="deskripsi" value="{{ $fasilitas['deskripsi'] }}">
                                {{ ($errors->has('dekripsi')) ? $errors->first('deskripsi') : "" }}
                            </div>
                        </div>
                        <br>
					<div class="row">
						<div class="col-md-3 offset-md-5 offset-md-4">
							<button type="submit" class="btn btn-outline-primary">Simpan</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
